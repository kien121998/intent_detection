from model import *
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
import json
import numpy as np
import model
import os
from flask_cors import CORS, cross_origin
from tensorflow import keras
from modelVoice import *
import requests
import os

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

UPLOAD_FOLDER = './uploads'
UPLOAD_TRAIN = './uploads_train'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['UPLOAD_TRAIN'] = UPLOAD_TRAIN


model = keras.models.load_model('./weights/TFIDF-keras.h5')
tfidf_pickle = load_tfidf_vectorizer()

#global parameters for Voice Identification
data_audio = []
data_own = None
_rfc, _pca = [], []

@app.route("/", methods=["GET"])  
@cross_origin()
def _hello_world():
	return "Hello world"


@app.route("/health", methods=["GET"])
@cross_origin()
def health():
	return  json.dumps({'status':'OK'})


#route for testing upload audio
@app.route("/testUploadAudio", methods=["POST"])
@cross_origin()
def testUploadAudio():
    data = {"status": 'NotOK'}
    print(request.files)
    if request.files:
        audio = request.files["audio"]
        filename = secure_filename(audio.filename)
        #filename = secure_filename(audio.filename + '.mp3')
        audio.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        print("Upload Audio Succesfully. Saved to upload folder")
        data["status"] = "OK"
    return json.dumps(data, ensure_ascii=False)

#Route for predict Intent from AUDIO
#input: AUDIO
#output: {"status":"OK", "class":'intent classes'}
@app.route("/predictIntent", methods=["POST"])
@cross_origin()
def predict():
    data = {"status": "NotOK"}
    if request.files:
        audio = request.files["audio"]
        filename = secure_filename(audio.filename)

        #############PROCESS INTENT DETECTION##################
        #url of audio file
        url_audio = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        audio.save(url_audio)

        #call function Speech2Text 
        sentences = voice_to_text_FPT(url_audio)
        print("sentences", sentences, "type:", type(sentences))
        classes = predict_intent_tfidf(sentences,tfidf_pickle, model)
        data["class"] = classes
        data["status"] = "OK"

    return json.dumps(data, ensure_ascii=False)

#route for add audio data


#Input : audio
#output : {"status": "OK"}
@app.route("/data", methods=["POST"])
@cross_origin()
def addTrainingAudio():
    data = {"status": 'NotOK'}
    print(request.files)
    print("USERNAME",request.form["username"])
    for f in os.listdir('./uploads_train'):
        os.remove(os.path.join('./uploads_train', f))
    try:
        audio = request.files["audio"]
        user_name = request.form["username"]
        print('Audio is uploaded')
        audio.save(os.path.join(app.config['UPLOAD_TRAIN'], 'audio.mp3'))

        data_own = loadAudioFolder('./uploads_train')
        addData2Mean(data_own,user_name)
        data["status"] = "OK"
        data["username"]  = user_name
    except:
        print("Error in uploading file")
    return json.dumps(data, ensure_ascii=False)

#route for trainning model voice-identification
#input : data-form with "username": Name of user. This function triggers train proccess with data saved in data_audio
#ouput: {"status": "OK"}
# @app.route("/train", methods=["POST"])
# @cross_origin()
# def trainData():
#     data = {"status": "NotOK"}
#     if request.files:
#         user_name = request.files["username"]
#     try:
#         data_own = loadAudioFolder('./uploads_train')
#         addData2Mean(data_own,user_name)
#         data = {"status": 'OK'}
#         return json.dumps(data, ensure_ascii=False)
#     except:
#         print("Error occured")
#         return json.dumps(data, ensure_ascii=False)

#route for voice identification 
#input : Audio
#ouput: {"status": "OK", "user": user_name} 
@app.route("/voiceIdentification", methods=["POST"])
@cross_origin()
def voiceIdentification():
    data = {"status": "Not ok"}
    if request.files:
        audio = request.files["audio"]
        filename = secure_filename(audio.filename)
        audio.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        data_x_vector = extract_Xvector(UPLOAD_FOLDER+'/'+filename)
        if(_rfc != None and _pca != None):
            label_predict = predict_ver2(_rfc[0], _pca[0], data_x_vector)
        else:
            print("rfc and pca are Empty")

        print("Hi "+ label_predict)
        data["user"] = label_predict

        data["status"] = 'OK'

    return json.dumps(data, ensure_ascii=False)

#Function get Text from voices (using FPT api)
#input : Audio_url(text)
#ouput: 'txt'
def voice_to_text_FPT(audio_url):
    url = 'https://api.fpt.ai/hmi/asr/general'
    payload = open(audio_url, 'rb').read()
    headers = {
        'api-key': 'ZWA4PFzx1ClIR8F5Cr0DMII7ONdnUH0r'
    }
    response = requests.post(url=url, data=payload, headers=headers)
    return response.json()['hypotheses'][0]['utterance']

#Function convert text to speech
#Input: Text
#Output: Link to file MP3(saved in server of FPT)
def text_to_voice_FPT(your_text):
    url = 'https://api.fpt.ai/hmi/tts/v5'

    payload = your_text
    headers = {
        'api-key': 'ZWA4PFzx1ClIR8F5Cr0DMII7ONdnUH0r',
        'speed': '',
        'voice': 'banmai'
    }

    response = requests.request('POST', url, data=payload.encode('utf-8'), headers=headers)
    return response['async']

#Function get answer from intent of specific user_name.Data get from data.json file
#input: userName and intent
#output: Answer with type String
def get_info_from_json(user_name, intent_class):
    # Opening JSON file
    f = open('data.json', encoding="utf8")
    # returns JSON object as
    # a dictionary
    data = json.load(f)
    answer_from_intent = data[user_name][intent_class]
    return answer_from_intent


#Route for predict ALL IN ONE
#input: AUDIO
#output: {"status":"OK", "class":'intent classes', "user":"user_name"}
@app.route("/predict", methods=["POST"])
@cross_origin()
def predictAIO():

    data = {"status": "notOK"}
    if request.files:
        audio = request.files["audio"]
        filename = secure_filename(audio.filename)

        #############PROCESS INTENT DETECTION##################
        #url of audio file
        url_audio = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        audio.save(url_audio)

        #call function Speech2Text 
        sentences = voice_to_text_FPT(url_audio)
        print("sentences", sentences, "type:", type(sentences))
        classes = predict_intent_tfidf(sentences,tfidf_pickle, model)
        data["class"] = classes

        ###########PROCESS VOICE IDENTIFICATION##################
        data_x_vector = extract_Xvector(url_audio)
        print('input data shape', data_x_vector.shape)
        print('Predicting....')
        label_predict = predictVoice(data_x_vector.cpu().detach().numpy())
            
        print("Hi "+ label_predict)
        data["user"] = label_predict
        data["action"] = get_info_from_json(label_predict, classes)
        data["status"] = "OK"

    return json.dumps(data, ensure_ascii=False)

if __name__ == "__main__":
    print("App run!")
    app.run(debug=False, host="0.0.0.0", threaded=False)
